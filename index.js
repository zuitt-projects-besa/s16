

function displayMsgToSelf() {
	console.log("Hi! Goodluuck! XD");
};

/*displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();*/

let count = 10;

while (count !== 0){
	displayMsgToSelf();
	count--;
};

// While Loop
	// While loop will allow us to repeat an instruction as long as the condition is true.
	/*
		Syntax:
		while(condition is true){
			statement/instruction runs
			incrementation or decrementation to stop the loop
		};
	*/

let number = 5;

while (number !== 0){
	console.log(`While ${number}`);
	number--;
};

while (number !== 11){
	console.log(`While ${number}`);
	number++;
};

// Do-While Loop
console.log("Do-While Loop");
	// A do-while loops works a lot like a while loop, but unlike while loop, do-while guarantees to run at least once
	/*
		Syntax:
		do {
			statement
		} while (condition)
	*/

// Number function works like parseInt, it change a string type to a number data type.
let number2 = Number(prompt("Give me a number"));

do {
	// the current value of the number2 is printed out
	console.log(`Do While ${number2}`)

	// increases the number by 1 after every iteration of the loop
	number2 += 1;

	// while(condition)
} while (number < 10);



let counter = 1;

do {
	console.log(counter);
	counter++;
} while (counter <= 21);

// For Loop
	// A more flexible loop than while and do-while
	// It consist of three parts: initialization, condition, finalExpression
		// Initialization - determine where the loop starts
		// Condition - determine when the loop stops
		// finalExpression - indicates how to advance the loop (incrementation or decrementation)
	/*
		Syntax:
		for (initialization; condition; finalExpression){
			statement;	
		};
	*/

console.log("For Loop");

for (let count = 0; count <=20; count++){
	console.log(count);
};

let myString = "alex";
console.log(myString.length);
	// result: 4
	// note: strings are special compared to other data types
	// it has access to functions and other pieces of information another primitive might not have
	// myString = ["a", "l", "e", "x"];

	console.log(myString[0]);
		// result: a
	console.log(myString[3]);
		// result: x

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
};

let myName = "Vincent";

for (let i = 0; i < myName.length; i++){
	if (

		myName[i].toLowerCase()== "a" ||
		myName[i].toLowerCase()== "e" ||
		myName[i].toLowerCase()== "i" ||
		myName[i].toLowerCase()== "o" ||
		myName[i].toLowerCase()== "u"

		){

		console.log(3);

	} else {
		console.log(myName[i]);
	};
};

// Continue and Break Statements
console.log("Continue and Break Statements");

for (let count = 0; count <= 20; count++){

	if (count % 2 === 0) {
		continue;
	}

	console.log(`Continue and Break ${count}`);

	if (count > 10) {
		break;
	}

};


let name = "alexandro";

for (let i = 0; i < name.length; i++){

	console.log(name[i]);

	if (name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration")
		continue;
	}


	if (name[i].toLowerCase() == "d") {
		break;
	}


}